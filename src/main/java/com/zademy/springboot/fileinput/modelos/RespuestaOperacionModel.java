package com.zademy.springboot.fileinput.modelos;

import java.io.Serializable;

/**
 * The Class RespuestaOperacionModel.
 */
public class RespuestaOperacionModel implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1793052093561825995L;

	/** The respuesta. */
	private boolean respuesta;

	/** The descripcion. */
	private String descripcion;

	/**
	 * Instantiates a new respuesta operacion model.
	 */
	public RespuestaOperacionModel() {
		super();
	}

	/**
	 * Checks if is respuesta.
	 *
	 * @return true, if is respuesta
	 */
	public boolean isRespuesta() {
		return respuesta;
	}

	/**
	 * Sets the respuesta.
	 *
	 * @param respuesta the new respuesta
	 */
	public void setRespuesta(boolean respuesta) {
		this.respuesta = respuesta;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RespuestaOperacionModel [respuesta=").append(respuesta).append(", descripcion=")
				.append(descripcion).append("]");
		return builder.toString();
	}

}
