package com.zademy.springboot.fileinput;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFileinputApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFileinputApplication.class, args);
	}

}
