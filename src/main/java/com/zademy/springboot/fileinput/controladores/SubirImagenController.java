package com.zademy.springboot.fileinput.controladores;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zademy.springboot.fileinput.modelos.RespuestaOperacionModel;
import com.zademy.springboot.fileinput.servicios.GuardarImagenService;

/**
 * @author Sadot
 *
 */
@Controller
public class SubirImagenController {

	/** Logger. */
	private static final Logger logger = LoggerFactory.getLogger(SubirImagenController.class);

	@Autowired
	@Qualifier("guardarImagenService")
	private GuardarImagenService guardarImagenService;

	/**
	 * @return index
	 */
	@GetMapping(path = "/")
	public String inicio() {

		return "index";
	}

	/**
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 */
	@PostMapping(path = "/subirImagen")
	@ResponseBody
	public RespuestaOperacionModel subirImagen(@RequestBody @RequestParam("file") MultipartFile file) {

		RespuestaOperacionModel respuesta = new RespuestaOperacionModel();

		respuesta.setRespuesta(true);

		try {
			logger.info("Empieza proceso para subir imagen");
			guardarImagenService.guardarImagen("C:\\Imagenes\\", file);
			respuesta.setDescripcion("Imagen subida correctamente");
		} catch (Exception e) {
			logger.error("Error al subir imagen: -> {0}", e);
			respuesta.setRespuesta(false);
			respuesta.setDescripcion("Error al subir imagen");
		}

		return respuesta;
	}

}
