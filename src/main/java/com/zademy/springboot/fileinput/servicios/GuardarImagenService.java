package com.zademy.springboot.fileinput.servicios;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

/**
 * The Interface GuardarImagenService.
 */
public interface GuardarImagenService {

	/**
	 * Guardar imagen.
	 *
	 * @param ruta the ruta
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void guardarImagen(String ruta, MultipartFile file) throws IOException;

}


