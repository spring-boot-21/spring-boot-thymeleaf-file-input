package com.zademy.springboot.fileinput.servicios.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.zademy.springboot.fileinput.servicios.GuardarImagenService;

/**
 * The Class GuardarImagenServiceImpl.
 */
@Service("guardarImagenService")
public class GuardarImagenServiceImpl implements GuardarImagenService {

	/**
	 * Guardar imagen.
	 *
	 * @param ruta the ruta
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void guardarImagen(String ruta, MultipartFile file) throws IOException {
		Path path = Paths.get(ruta, file.getOriginalFilename());
		Files.write(path, file.getBytes());

	}

}
