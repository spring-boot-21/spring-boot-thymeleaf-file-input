
$('#file-0a').fileinput({
	theme: 'fas',
	language: 'es',
	showUpload: true,
	showCaption: true,
	uploadAsync: true,
	maxFileSize: 1768,
	allowedFileExtensions: ['jpg', 'png', 'gif'],
	uploadUrl: '/subirImagen',
	elErrorContainer: '#kv-error-1'
}).on('filebatchpreupload', function(event, data, id, index) {
	$('#kv-success-1').html('<h4>Estatus </h4><ul></ul>').hide();
}).on('fileuploaded', function(event, data, id, index) {
	var fname = data.files[index].name,
		out = '<li>' + 'Archivo # ' + (index + 1) + ' - ' + fname + ' subido exitosamente.' + '</li>';
	$('#kv-success-1 ul').append(out);
	$('#kv-success-1').fadeIn('slow');
});