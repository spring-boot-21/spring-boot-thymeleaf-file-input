## Pre-requisitos
- Spring Boot 2.3.4.RELEASE
- Spring 5.2.9.RELEASE
- Thymeleaf 3.0.11.RELEASE
- Tomcat embed 9.0.38
- Gradle 6.7
- Java 11

## Construir proyecto
```
gradle build
```

## Ejecutar proyecto
```
gradle run
```

Accede a la página utilizando la URL:
- http://localhost:8080/

